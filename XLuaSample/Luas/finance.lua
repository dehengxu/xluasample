#!/usr/bin/env lua

function compoundInterestIncreasebasePerMonth(base, increase, interest, monthes)
    if (monthes == 0) then
        return base
    else
        return compoundInterestIncreasebasePerMonth(base + (base * interest) / 12.0 + increase, increase, interest, monthes - 1)
    end
end

function ComputeMoneyAfterMonth(base, increase, interest, monthes)
    print("\n\n~~~")
    print(string.format("利率 :%f%%",(interest * 100.0)))
    if (monthes < 12) then
    print(string.format("投资%d个月", (monthes)))
    else
        print(string.format("投资%f年", (monthes / 12.0)))
    end
    -- 一年后本息金额
    money = compoundInterestIncreasebasePerMonth(base, 0, interest, monthes)
    print "~~~"
    print(string.format("无增量本息 :%f", (money)))
    print(string.format("无增量本金 :%f", (base + 0 * monthes)))
    print(string.format("无增量利息 :%f",(money - (base + 0 * monthes))))

    --  一年后本息金额 和 本金
    money = compoundInterestIncreasebasePerMonth(base, increase, interest, monthes)
    print "~~~"
    print(string.format("本息 :%f", (money)))
    print(string.format("本金 :%f", (base + increase * monthes)))
    print(string.format("利息 :%f", (money - (base + increase * monthes))))
    if  ((money - (base + increase * monthes)) > (base + increase * monthes)) then
        print "利息超过本金"
    end
    return money

end

base = 80000
increase = 6400
interest = 0.045
desire = 100 * 10000
local years = 5
local monthes = 12 * years

local i = 1
while(i < 31) do
    result = ComputeMoneyAfterMonth(base, increase, interest, i * 12)

    if (result > desire) then
        print()
        print(string.format("经过 %d 年，本息%f 万， 已超过100万!", i, (result / 10000.0)))
        break
    end

    i = i+ 1
end
