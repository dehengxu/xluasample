//
//  CPLabs.h
//  XLuaSample
//
//  Created by NicholasXu on 13-4-20.
//  Copyright (c) 2013年 NicholasXu. All rights reserved.
//

#ifndef __XLuaSample__CPLabs__
#define __XLuaSample__CPLabs__

#include <iostream>

#include "XLua.h"

class RMB;

class CPPLab {
    XLua lua;
    
public:
    CPPLab();
    virtual ~CPPLab();
    
    void RunTest();
    void RunTestLuaFile(const char *path);
    void RunTestOperator();
    
    void ShownMoney(RMB rmb);
};

class RMB {
private:
    double money;
    
    long yuan;
    long jiao;
    long fen;
    
public:
    RMB();
    RMB(double money);
    virtual ~RMB();
    
    void setMoney(double nMoney);
    double getMoney();
    
    const char *content();
    
    //Operator overide
    RMB operator + (RMB rmb1);
    
    //Friends
    friend RMB operator * (RMB rmb1, RMB rmb2);
    //指定 CPPLab 类的 ShownMoney 方法为友元方法
    friend void CPPLab::ShownMoney(RMB rmb);
    friend class CPPLab;
};

#endif /* defined(__XLuaSample__CPLabs__) */
