//
//  CPLabs.cpp
//  XLuaSample
//
//  Created by NicholasXu on 13-4-20.
//  Copyright (c) 2013年 NicholasXu. All rights reserved.
//

#include "CPLabs.h"
#include "XLua.h"

#pragma mark - CPPLab

CPPLab::CPPLab()
{
}

CPPLab::~CPPLab()
{
    
}

void CPPLab::RunTest()
{
}

void CPPLab::RunTestLuaFile(const char *path)
{
    lua.RunLuaInString("x = 19");
    lua.RunLuaInString("y = 29");
//    for (int i = 0; i < 1; i ++) {
        lua.RunLuaInString("print (\"世界，你好! Lua is running in iOS!\")");
        lua.RunLuaInString("print (\"x + y = \", (x + y))");
        lua.RunLuaInFile(path);
//    }

}

void CPPLab::RunTestOperator()
{
    RMB rmb1, rmb2;
    rmb1.setMoney(122.33);
    rmb2.setMoney(837.22);
    RMB rmb = rmb1 + rmb2;
    printf("rmb %f + %f = %f\n", rmb1.getMoney(), rmb2.getMoney(), rmb.getMoney());
    
    rmb = rmb1 * rmb2;
    printf("rmb %f * %f = %f\n", rmb1.getMoney(), rmb2.getMoney(), rmb.getMoney());
    
}

void CPPLab::ShownMoney(RMB rmb)
{
    printf("%s %f", __FUNCTION__, rmb.money);
}

#pragma mark - RMB

RMB::RMB()
{
    
}

RMB::RMB(double money)
{
    setMoney(money);
}

RMB::~RMB()
{
    
}

void RMB::setMoney(double nMoney)
{
    money = nMoney;
    long tmp = ((long)nMoney * 100) % 100;
    yuan = tmp % 1000;
    jiao = tmp % 100;
    fen = tmp % 10;
}

double RMB::getMoney()
{
    return money;
}

const char * RMB::content()
{
    return "";
}

RMB RMB::operator+(RMB rmb1)
{
    RMB rmbResult;
    rmbResult.setMoney(money + rmb1.money);
    return rmbResult;
}

RMB operator*(RMB rmb1, RMB rmb2)
{
    RMB rmbResult;
    rmbResult.setMoney(rmb1.money * rmb2.money);
    return rmbResult;
}

