//
//  XLua.m
//  XLuaSample
//
//  Created by NicholasXu on 13-4-20.
//  Copyright (c) 2013年 NicholasXu. All rights reserved.
//

#import "XLuaTouch.h"

@implementation XLuaTouch

- (void)dealloc
{
    if (LState) {
        lua_close(LState);
    }
    [super dealloc];
}

- (lua_State *)LuaState
{
    if (NULL == LState) {
        LState = luaL_newstate();
        luaL_openlibs(LState);
    }
    return LState;
}

- (void)reloadLuaStack
{
    if (LState) {
        lua_close(LState);
        LState = NULL;
    }
    [self LuaState];
}

- (void)runLuaString:(NSString *)lusString
{
    int error = 0;
    lua_settop(self.LuaState, 0);
    error = luaL_loadstring(self.LuaState, [lusString cStringUsingEncoding:NSUTF8StringEncoding]);
    
    if (error != 0) {
        NSLog(@"load error!");
        return;
    }
    
    error = lua_pcall(self.LuaState, 0, LUA_MULTRET, 0);
    if (error != 0) {
        NSLog(@"run error!");
        return;
    }
    
    int nresults = lua_gettop(self.LuaState);
    if (nresults == 0) {
        NSLog(@"run successed!");
    }else {
        for (int i = 0; i < nresults; i++) {
            const char *msg = lua_tostring(self.LuaState, 0);
            NSLog(@"%d :%s", i, msg);
        }
        lua_pop(self.LuaState, nresults);
    }
}

- (void)runLuacString:(const char *)cString
{
    int error = 0;
    lua_settop(self.LuaState, 0);
    error = luaL_loadstring(self.LuaState, cString);
    if (error) {
        return;
    }
}

- (void)runLuaInFile:(NSString *)path
{
    int error = 0;
    lua_settop(self.LuaState, 0);
    luaL_loadfile(self.LuaState, [path cStringUsingEncoding:NSUTF8StringEncoding]);

    if (error) {
        return;
    }
}

@end
