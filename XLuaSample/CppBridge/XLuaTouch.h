//
//  XLua.h
//  XLuaSample
//
//  Created by NicholasXu on 13-4-20.
//  Copyright (c) 2013年 NicholasXu. All rights reserved.
//

#import <Foundation/Foundation.h>

#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"

@interface XLuaTouch : NSObject
{
    lua_State *LState;
}

- (lua_State *)LuaState;
- (void)reloadLuaStack;
- (void)runLuaString:(NSString *)lusString;
- (void)runLuacString:(const char *)cString;
- (void)runLuaInFile:(NSString *)path;

@end
