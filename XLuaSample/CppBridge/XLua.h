//
//  XLua.h
//  XLuaSample
//
//  Created by NicholasXu on 13-4-20.
//  Copyright (c) 2013年 NicholasXu. All rights reserved.
//

#ifndef __XLuaSample__XLua__
#define __XLuaSample__XLua__

#include <iostream>

#define RUN_LOG 1

extern "C" {
    #include "lua.h"
    #include "lualib.h"
    #include "lauxlib.h"
}

class XLua {
private:
    lua_State *LState;
    
protected:
    
public:
    
    XLua();
    virtual ~XLua();
    
    //Lua feature.
    lua_State *GetLuaState();
    void ReloadLuaStack();
    void RunLuaInString(const char *luaString);
    void RunLuaInFile(const char *filePath);
    
    //Debug
    inline void FetchError();
};

#endif /* defined(__XLuaSample__XLua__) */

