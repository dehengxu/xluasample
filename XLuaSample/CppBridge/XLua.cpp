//
//  XLua.cpp
//  XLuaSample
//
//  Created by NicholasXu on 13-4-20.
//  Copyright (c) 2013年 NicholasXu. All rights reserved.
//

#include "XLua.h"

XLua::XLua()
{
    LState = NULL;
    printf("%s Created!  %d\n", __FUNCTION__, LState);
}

XLua::~XLua()
{
    printf("%s Dealloced!\n", __FUNCTION__);
    if (LState != NULL) {
        lua_close(LState);
    }
}

lua_State *XLua::GetLuaState()
{
    if (NULL == LState) {
        LState = luaL_newstate();
        luaL_openlibs(LState);
    }
    return LState;
}

void XLua::ReloadLuaStack()
{
    lua_settop(GetLuaState(), 0);
}

void XLua::RunLuaInString(const char *luaString)
{
    int error = 0;
    error = luaL_loadstring(GetLuaState(), luaString);
    
    if (error != 0) {
        printf("load error!\n");
        FetchError();
        return;
    }
    
    error = lua_pcall(GetLuaState(), 0, LUA_MULTRET, 0);
    if (error != 0) {
        printf("run error!\n");
        FetchError();
        return;
    }
    
    FetchError();
}

void XLua::RunLuaInFile(const char *filePath)
{
    printf("%s\n", filePath);
    int error = 0;
    error = luaL_loadfile(GetLuaState(), filePath);
    if (error != 0) {
        printf("load error! %d\n", error);
        FetchError();
        return;
    }
    
    error = lua_pcall(GetLuaState(), 0, LUA_MULTRET, 0);
    if (error != 0) {
        printf("run error! %d\n", error);
        FetchError();
        return;
    }
    
    FetchError();
}

inline void XLua::FetchError()
{
    int nresults = lua_gettop(GetLuaState());
    if (nresults == 0) {
        printf("run succeeded!\n");
    }else {
        for (int i = 0; i < nresults; i++) {
            const char *msg = lua_tostring(GetLuaState(), 0);
            printf("Error %d :%s\n", i, msg);
        }
        lua_pop(GetLuaState(), nresults);
    }
}
