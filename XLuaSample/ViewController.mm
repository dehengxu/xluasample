//
//  ViewController.m
//  XLuaSample
//
//  Created by NicholasXu on 13-4-20.
//  Copyright (c) 2013年 NicholasXu. All rights reserved.
//

#import "ViewController.h"

#import "XLuaTouch.h"
#import "XLua.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    //cppLab.RunTestOperator();
//    NSDate *start = [NSDate date];
//    cppLab.RunTestLuaFile([[[NSBundle mainBundle] pathForResource:@"lablua.lua" ofType:nil] cStringUsingEncoding:NSUTF8StringEncoding]);
//    NSLog(@"lua cost time :%f(s)", [[NSDate date] timeIntervalSinceDate:start]);
    XLua lua;
    
    NSString *luaContent = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"finance.lua" ofType:nil] encoding:NSUTF8StringEncoding error:nil];
    NSLog(@"%@", luaContent);
    lua.RunLuaInString([luaContent cStringUsingEncoding:NSUTF8StringEncoding]);
    

    NSString *path = [[NSBundle mainBundle] pathForResource:@"lablua.lua" ofType:nil];
    const char *cPath = [path cStringUsingEncoding:NSUTF8StringEncoding];
    
    NSDate *start = [NSDate date];
    XLua xlua;
    xlua.RunLuaInFile(cPath);
    
//    XLuaTouch *luatouch = [[XLuaTouch alloc] init];
//    [luatouch runLuaString:path];

    NSLog(@"lua cost time :%f(s)", [[NSDate date] timeIntervalSinceDate:start]);
}

- (void)viewWillAppear:(BOOL)animated
{

}

- (void)viewDidAppear:(BOOL)animated
{
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
